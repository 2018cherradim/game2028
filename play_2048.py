from game2048.grid_2048 import *
from game2048.textual_2048 import *

import random

#RANDOM PLAY
def randow_play():
    default_size=4
    default_theme=THEMES["0"]
    list_of_choices=["left","right","up","down"]

    grid=create_grid(default_size)
    grid_add_new_tile(grid)
    grid_add_new_tile(grid)
    print(grid_to_string(grid))
    while not is_game_over(grid):
        d=random.choice(list_of_choices)
        grid=move_grid(grid,d)
        fill_the_tiles(grid,d)
        print(grid_to_string_with_size(grid))
#random_play()



#PLAY
def game_play():
    size_grid=read_size_grid()
    theme=read_theme_grid()
    if theme=="d":
        theme=THEMES["0"]
    if theme=="c":
        theme=THEMES["1"]
    if theme=="a":
        theme=THEMES["2"]
    grid=create_grid(int(size_grid))
    grid_add_new_tile(grid)
    grid_add_new_tile(grid)
    print(grid_to_string_with_size_and_theme(grid,theme))
    while not is_game_over(grid):
        d=read_player_command()
        if d=='g':
            d='left'
        if d=='d':
            d='right'
        if d=='h':
            d='up'
        if d=='b':
            d='down'
        grid=move_grid(grid,d)
        fill_the_tiles(grid,d)
        print(grid_to_string_with_size_and_theme(grid,theme))
    if get_grid_tile_max(grid)>2047:
        print("Vous avez gagné!")
    else:
        print("Vous avez perdu.")

game_play()


