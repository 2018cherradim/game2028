import tkinter as tk
from functools import partial
from tkinter import Tk, StringVar, Label, Entry, Button
from functools import partial




# def write_text():
#     print("Hello CentraleSupelec")
#
# root = tk.Tk()
# frame = tk.Frame(root)
# frame.pack()
#
# button = tk.Button(frame,
#                    text="QUIT",
#                    activebackground = "blue",
#                    fg="red",
#                    command=quit)
# button.pack(side=tk.LEFT)
# slogan = tk.Button(frame,
#                    fg="blue",
#                    text="Hello",
#                    command=write_text)
# slogan.pack(side=tk.LEFT)
#
# root.mainloop()


#def update_label(label, stringvar):
#     """
#     Met à jour le texte d'un label en utilisant une StringVar.
#     """
#     text = stringvar.get()
#     label.config(text=text)
#     stringvar.set('merci')
#
# root = Tk()
# text = StringVar(root)
# label = Label(root, text='Your name')
# entry_name = Entry(root, textvariable=text)
# button = Button(root, text='clic',
#                 command=partial(update_label, label, text))
#
# label.grid(column=0, row=0)
# entry_name.grid(column=0, row=1)
# button.grid(column=0, row=2)
#
# root.mainloop()

from tkinter import Tk, Label

# root = Tk() # Création de la fenêtre racine
# label = Label(root, text='Hello')
# label2= Label(root)
# label.grid()
# label2.grid(column=20,row=20)
# root.mainloop() # Lancement de la boucle principale

from tkinter import *
from functools import partial


# root = Tk()
# root.title("2048")
# n=len([[2,0,0,2], [2, 4, 0, 0], [8, 4, 2, 0], [8, 2, 2, 0]])
# window=Toplevel(root,height=100*n,width=100*n)
# for i in range(n):
#     for j in range(n):
#         frame= Frame(window,borderwidth=2,relief=GROOVE,bg="#776e65")
#         Label(frame,padx=100,pady=100).grid(row=i,column=j)
#         frame.place(x=i*100,y=j*100)
# window.mainloop()


from tkinter import Tk, Label, Frame

# root = Tk()
# f1 = Frame(root, bd=1, relief='solid')
# f2= Frame(root, bd=1, relief='solid')
# Label(f1, text='je suis dans F1').grid(row=0, column=0)
# Label(f2, text='moi aussi dans F1').grid(row=0, column=1)
# root.title("2048")
# f1.grid(row=0, column=0)
# f2.grid(row=3, column=3)
# Label(root, text='je suis dans root').grid(row=1, column=0)
# Label(root, text='moi aussi dans root').grid(row=2, column=0)
#
# root.mainloop()

# from tkinter import *
# from pprint import pformat
#
# def print_bonjour(i):
#     label.config(text="Hello")
#
# root = Tk()
# frame = Frame(root, bg='white', height=100, width=400)
# entry = Entry(root)
# label = Label(root)
#
# frame.grid(row=0, column=0)
# entry.grid(row=1, column=0, sticky='ew')
# label.grid(row=2, column=0)
#
# frame.bind('<ButtonPress>', print_bonjour)
# entry.bind('<KeyPress>', print_bonjour)
# root.mainloop()
#
#
# from tkinter import *
# from pprint import pformat
# from functools import partial
#
# def print_event(label, event):
#     label.config(text=pformat(vars(event)))
#
# root = Tk()
# frame = Frame(root, bg='white', height=100, width=400)
# entry = Entry(root)
# label = Label(root)
#
# frame.grid(row=0, column=0)
# entry.grid(row=1, column=0, sticky='ew')
# label.grid(row=2, column=0)
#
# frame.bind('<ButtonPress>', partial(print_event, label))
# entry.bind('<KeyPress>', partial(print_event, label))
#root.mainloop()

from tkinter import *

# def change_focus(event):
#     current_row = event.widget.grid_info()['row']
#     parent = event.widget.master
#     next_widgets = parent.grid_slaves(column=0, row=current_row + 1)
#     if next_widgets:
#         next_widgets[0].focus()
#
# root = Tk()
# entry_group = Frame(root)
# for i in range(3):
#     entry = Entry(entry_group)
#     entry.grid(row=i, column=0)
#     entry.bind('<Key-Return>', change_focus)
#     entry_group.grid(row=0, column=0)
#
# root.mainloop()

# import tkinter as tk
#
# def on_double_click(event):
#     print("Position de la souris:", event.x, event.y)
#
# app = tk.Tk()
# app.bind("<Double-Button-1>", on_double_click)
# app.mainloop()
#
# from random import randint
# import tkinter as tk
#
# def nombre_choisi(event):
#     "Callback quand le joueur a entré un nombre."
#     nbre_choisi = int(reponse.get())
#     reponse.delete(0, tk.END)
#     proposition["text"] = nbre_choisi
#     if nombre_secret > nbre_choisi:
#         resultat["text"] = "Le nombre est plus grand"
#     elif nombre_secret < nbre_choisi:
#         resultat["text"] = "Le nombre est plus petit"
#     else:
#         # On enlève les éléments dont on n'a plus besoin
#         lbl_reponse.destroy()
#         reponse.destroy()
#         # On replace les Labels `proposition` et `resultat` dans la ligne
#         # en dessous du titre
#         proposition.grid_forget()
#         proposition.grid(row=1, column=0)
#         resultat.grid_forget()
#         resultat.grid(row=1, column=1)
#         # On configure le label avec le texte voulu, dans la font voulue et
#         # dans la couleur désirée.
#         resultat.config(text="Tu as trouvé le nombre. Bravo!",
#                         font=("", 12),
#                         fg="green")
#
#
# app = tk.Tk()
# titre = tk.Label(app, text="Devine le nombre auquel je pense", font=("", 16))
# titre.grid(row=0, columnspan=2, pady=8)
#
# nombre_secret = randint(0, 100) + 1
#
# lbl_reponse = tk.Label(app, text="Choisi un nombre entre 1 et 100 inclus:")
# lbl_reponse.grid(row=1, column=0, pady=5, padx=5)
#
# reponse = tk.Entry(app)
# reponse.grid(row=1, column=1, pady=5, padx=5)
# reponse.bind("<Return>", nombre_choisi)
#
# proposition = tk.Label(app, text="")
# proposition.grid(row=2, column=0, pady=5, padx=5)
#
# resultat = tk.Label(app, text="")
# resultat.grid(row=2, column=1, pady=5, padx=5)
#
# app.mainloop()

# import tkinter as tk
# k=0
# text='Hello world'
# def keypressed(event):
#     global k
#     global root
#     k+=1
#     global text
#     text+='hallo'
#     print('hellow')
#     print(k)
#     if k==4:
#         root.quit()
#
# root=tk.Tk()
# root.title(text)
# root.bind('<Key>',keypressed)
# root.mainloop()
#

# from tkinter import *
# from tkinter import ttk
#
# root = Tk()
#
# textbox = Text(root, width=60, height=3)
# textbox.grid(sticky=(N, S, E, W))
#
# def KeyboardEvent(event):
#     if event.keysym_num > 0 and event.keysym_num < 60000:
#         print('This is a printable key. The character is: %r keysym: %r' % \
#             (event.char, event.keysym_num))
#     else:
#         print('This key is unprintable. The character is: %r keysym: %r' % \
#             (event.char, event.keysym_num))
# textbox.bind('<KeyPress>', KeyboardEvent)
# root.mainloop()
#

