#Fonctionnalité 3

def read_player_command():
    move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)):")
    while not move in ["g","d","h","b"]:
        move=input("La commande n'est pas correcte. \n Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)):")
    return move


def read_size_grid():
    size_grid = input("Entrez la taille de grille souhaitée:")
    try:
        while not size_grid.isdigit() :
            size_grid = input("La commande n'est pas correcte \n Entrez la taille de grille souhaitée:")
    except ValueError:
        size_grid = input("La commande n'est pas correcte \n Entrez la taille de grille souhaitée:")
    return size_grid




def read_theme_grid():
    theme_grid=input("Entrez le theme souhaité (d (default), c (chemistry), a (alphabet)):")
    while not theme_grid in ["d","c","a"]:
        theme_grid=input("La commande n'est pas correcte. \n Entrez le theme souhaité (d (default), c (chemistry), a (alphabet)):")
    return theme_grid



