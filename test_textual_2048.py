from game2048.textual_2048 import *
from pytest import *
import random

def test_mock_inputreturn(monkeypatch):
    def mockreturn():
        return random.choice(["h","b","g","d"])
    monkeypatch.setath('builtins.input',mockreturn)
    move=read_player_command()
    assert move in ["h","b","g","d"]
    assert move != "t"

