import random


#Fonctionnalité 1

def create_grid(grid_size):
    """Crée la grille qui est l"espace de jeu"""
    game_grid = []
    for i in range(grid_size):
        game_grid.append([0,0,0,0])
    return game_grid


def get_value_new_tile():
    """Génère aléatoirement une valeur qui est soit 2 ou 4 pour remplir les tuiles qui apparaissent sur la grille"""
    return random.choices([2,4],[0.9,0.1])[0]


# def grid_add_new_tile_at_position(game_grid,positionx,positiony):
#     """Une tuile de valeur 2 est placée à une position donnée"""
#     game_grid[positionx][positiony]=get_value_new_tile()
#     return game_grid



def get_all_tiles(game_grid):
    """Retourne une liste des valeurs prises par chaque tuile de la grille. Une tuile vide a pour valeur 0"""
    all_tiles=[]
    game_grid_size=len(game_grid)
    for i in range(game_grid_size):
        for j in range(game_grid_size):
            if game_grid[i][j]==' ':
                all_tiles.append(0)
            else:
                all_tiles.append(game_grid[i][j])
    return all_tiles


def get_empty_tiles_positions(game_grid):
    """Retourne les positions de toutes les tuiles vides de la grille. Une grille vide a soit la valeur 0 soit la valeur ' ' """
    all_tiles=[]
    game_grid_size=len(game_grid)
    for i in range(game_grid_size):
        for j in range(game_grid_size):
            if game_grid[i][j]==' ' or game_grid[i][j]==0:
                all_tiles.append((i,j))
    return all_tiles


def get_new_position(game_grid):
    """Retourne les coordonnées x,y d'une tuile choisie aléatoirement parmi toutes les tuiles vides """
    #game_grid_size=len(game_grid)
    # list_possible_tiles=[]
    # for tile in get_empty_tiles_positions(game_grid):
    #     if tile[1]==0 or tile[1]==game_grid_size-1:
    #         list_possible_tiles.append(tile)
    chosen_tile=random.choice(get_empty_tiles_positions(game_grid))
    return chosen_tile[0],chosen_tile[1]


def grid_get_value(game_grid,x,y):
    """Retourne la valeur de la tuile en position x,y dans la grille game_grid"""
    if game_grid[x][y]==' ':
        return 0
    else:
        return game_grid[x][y]

# def grid_add_new_tile(grid):
#     """Ajoute une nouvelle tuile à la grille en respectant les règles d'ajout"""
#     grid_add_new_tile_at_position(grid,get_new_position(grid)[0],get_new_position(grid)[1])
#     return grid

def grid_add_new_tile(grid):
    """Ajoute une nouvelle tuile à la grille en respectant les règles d'ajout"""
    grid[get_new_position(grid)[0]][get_new_position(grid)[1]]=get_value_new_tile()
    return grid

def init_game(grid_size=4):
    """Crée une ouvelle grille de jeu comportant 2 tuiles pleines"""
    new_grid=create_grid(grid_size)
    grid_add_new_tile(new_grid)
    grid_add_new_tile(new_grid)
    return new_grid


# Fonctionnalité 2
def grid_to_string(grid):
    """Converti la grille qui est sous forme de liste de liste en grille sous forme de chaine de caractère"""
    size_grid=len(grid)
    grid_string="\n"
    for line in range(size_grid):
       for column in range(size_grid):
           grid_string+=" ==="
       grid_string+=" \n"
       for column in range(size_grid):
           grid_string+="| "+str(grid[line][column])+" "
       grid_string+="|\n"
       for column in range(size_grid):
           grid_string+=" ==="
       grid_string+=" \n"
    return grid_string

#print(grid_to_string([[' ', ' ', ' ', ' '], [' ', ' ', ' ', ' '], [' ', ' ', ' ', ' '], [2, ' ', ' ', 2]]))



def max_size_in_column(grid,column):
    """Renvoie la longueur de la chaine de caractère la plus longue parmi celles de la colonne column (équivalent de la fonction long_value"""
    max_size=1
    for line in range(len(grid)):
        if max_size<len(str(grid[line][column])):
            max_size=len(str(grid[line][column]))
    return max_size

def grid_to_string_with_size(grid):
    """Converti la grille qui est sous forme de liste de liste en grille sous forme de chaine de caractère"""
    size_grid=len(grid)
    grid_string="\n"
    list_of_column_sizes=[]
    for column in range(size_grid):
        list_of_column_sizes.append(max_size_in_column(grid,column))
    for line in range(size_grid):
       for column in list_of_column_sizes:
           grid_string+=" " +"=="+column*"="
       grid_string+=" \n"
       for column in range(size_grid):
           grid_string+="| "+str(grid[line][column])+(max_size_in_column(grid,column)-len(str(grid[line][column])))*" "+" "
       grid_string+="|\n"
       for column in list_of_column_sizes:
           grid_string+=" " +"=="+column*"="
       grid_string+=" \n"
    return grid_string


#print(grid_to_string([[' ', ' ', ' ', ' '], [' ', ' ', ' ', ' '], [' ', ' ', ' ', ' '], [2048, ' ', ' ', 2]]))

THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: " ", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}

# def long_value_with_theme(grid,theme):
#     size_grid=len(grid)
#     print(theme)
#     print(grid)
#     for line in range(size_grid):
#         for column in range(size_grid):
#             grid[line][column]=theme[grid[line][column]]
#     print(grid)
#     print(grid_to_string(grid))
#     print(max_size_in_column(grid,2))
#     long_value=0
#     for column in range(size_grid):
#         if long_value<max_size_in_column(grid,column):
#             long_value=max_size_in_column(grid,column)
#     return long_value
#
# long_value_with_theme([[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 4096], [1024, 2048, 512, 2]],THEMES["2"])
# long_value_with_theme([[2048, 16, 32, 0], [0, 4, 0, 2], [0, 0, 0, 32], [512, 1024, 0, 2]],THEMES["2"])
# print(long_value_with_theme([[2048, 16, 32, 0], [0, 4, 0, 2], [0, 0, 0, 32], [512, 1024, 0, 2]],THEMES["1"]))

def long_value_with_theme(grid,theme):
    """Renvoie la taille de la plus longue chaine de caractère dans une tuile de la grille avec le thème"""
    size_grid=len(grid)
    for line in range(size_grid):
        for column in range(size_grid):
            grid[line][column]=theme[grid[line][column]]
    long_value=0
    for column in range(size_grid):
        if long_value<max_size_in_column(grid,column):
            long_value=max_size_in_column(grid,column)
    print(long_value)
    return long_value

def grid_to_string_with_size_and_theme(grid,theme):
    """Transorme la grille sous forme de liste en grille sous forme de chaine de caractère en respectant le thème"""
    size_grid=len(grid)
    copy_grid=[line[:] for line in grid]
    for line in range(size_grid):
        for column in range(size_grid):
            copy_grid[line][column]=theme[grid[line][column]]
    return grid_to_string(copy_grid)

# print(grid_to_string_with_size_and_theme([[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 4096], [1024, 2048, 512, 2]],THEMES["2"]))
# print(grid_to_string_with_size_and_theme([[2048, 16, 32, 0], [0, 4, 0, 2], [0, 0, 0, 32], [512, 1024, 0, 2]],THEMES["2"]))
# print(grid_to_string_with_size_and_theme([[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 4096], [1024, 2048, 512, 2]],THEMES["1"]))
# print(grid_to_string_with_size_and_theme([[2048, 16, 32, 0], [0, 4, 0, 2], [0, 0, 0, 32], [512, 1024, 0, 2]],THEMES["1"]))
# print(grid_to_string_with_size_and_theme([[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 4096], [1024, 2048, 512, 2]],THEMES["0"]))
# print(grid_to_string_with_size_and_theme([[2048, 16, 32, 0], [0, 4, 0, 2], [0, 0, 0, 32], [512, 1024, 0, 2]],THEMES["0"]))


#Fonctionnalité 4:
def first_non_null_index_left(row):
    """Renvoie l'index de la première tuile non vide dans la ligne (de la gauche vers la droite) et la longueur de la ligne si elles sont toutes vides"""
    for tile_index in range(len(row)):
        if row[tile_index] != 0:
            return tile_index
    return len(row)


def move_row_left_without_sum(row):
    """Plaque toutes les tuile non vide vers la gauche"""
    if len(row)==1 or first_non_null_index_left(row)==len(row):
        return row
    else:
        x=first_non_null_index_left(row)
        row[0],row[x]=row[x],row[0]
        return [row[0]]+move_row_left_without_sum(row[1:])

def move_row_left(row):
    """Renvoie l'effet d'une commande "gauche" sur la grille"""
    row=move_row_left_without_sum(row)
    index=0
    while index<len(row)-1:
        if row[index]==row[index+1]:
            row[index]=2*row[index]
            row[index+1]=0
            index+=2
        else:
            index+=1
    return move_row_left_without_sum(row)

#Essais de la fonctionnalité 4 n'ayant pas marché (trop compliqués)
#print(move_row_left_without_sum([4, 8, 16, 32]))
#print(move_row_left_without_sum([0, 4, 0, 2]))


#print(first_non_null_index_left([0, 0, 0, 2]))
# def move_row_left(row):
#     iteration_number=0
#     row_iterate=row[iteration_number:]
#     first_non_null_index=first_non_null_index_left(row_iterate)
#     len_row=len(row_iterate)
#     while first_non_null_index<len_row:
#         row_iterate=row[iteration_number:]
#         len_row=len(row_iterate)
#         next_non_null_index=first_non_null_index_left(row_iterate[first_non_null_index+1:])
#         print(first_non_null_index)
#         print(row)
#         if next_non_null_index==len_row:
#             row_iterate[0]=row_iterate[first_non_null_index]
#             row_iterate[first_non_null_index]=0
#             break
#         elif row[next_non_null_index]==row[first_non_null_index]:
#             row_iterate[0]=row_iterate[first_non_null_index]+row_iterate[next_non_null_index]
#             row_iterate[first_non_null_index],row_iterate[next_non_null_index]=0,0
#             first_non_null_index=next_non_null_index+1
#             iteration_number+=1
#         else:
#             row_iterate[0],row_iterate[1]=row_iterate[first_non_null_index],row_iterate[next_non_null_index]
#             row_iterate[first_non_null_index],row_iterate[next_non_null_index]=0,0
#             first_non_null_index=next_non_null_index+1
#             iteration_number+=2
#     return row

# def move_row_left(row):
#     if len(row)==3:
#         return row
#     else:
#         first_non_null_index=first_non_null_index_left(row)
#         next_non_null_index=first_non_null_index_left(row[first_non_null_index+1:])
#         if next_non_null_index == len(row):
#             x=first_non_null_index_left(row)
#             row[0],row[x]=row[x],row[0]
#             return row[0:1] + move_row_left(row[1:])
#         elif row[next_non_null_index]==row[first_non_null_index]:
#             if first_non_null_index == 0:
#                 row[0],row[next_non_null_index]=row[0]+row[next_non_null_index],0
#                 return row[0:2]+move_row_left(row[1:])
#             else:
#                 row[0],row[first_non_null_index],row[next_non_null_index]=row[first_non_null_index]+row[next_non_null_index],0,0
#                 return row[0:2]+move_row_left(row[1:])
#         else:
#             if first_non_null_index == 0:
#                 if next_non_null_index==1:
#                     return row[0:3]+move_row_left(row[2:])
#                 else:
#                     row[1],row[next_non_null_index]=row[next_non_null_index],0
#                     return row[0:3]+move_row_left(row[2:])
#             else:
#                 row[0],row[1],row[first_non_null_index],row[next_non_null_index]=row[first_non_null_index],row[next_non_null_index],0,0
#                 return row[0:3]+move_row_left(row[2:])


# print(move_row_left())
#     assert move_row_left([0, 2, 0, 4]) == [2, 4, 0, 0]
#     assert move_row_left([2, 2, 0, 4]) == [4, 4, 0, 0]
#     assert move_row_left([2, 2, 2, 2]) == [4, 4, 0, 0]
#     assert move_row_left([4, 2, 0, 2]) == [4, 4, 0, 0]
#     assert move_row_left([2, 0, 0, 2]) == [4, 0, 0, 0]
#     assert move_row_left([2, 4, 2, 2]) == [2, 4, 4, 0]
#     assert move_row_left([2, 4, 4, 0]) == [2, 8, 0, 0]
#     assert move_row_left([4, 8, 16, 32]) == [4, 8, 16, 32]


    #
    # while first_non_null_index<len_row:
    #     next_non_null_index=first_non_null_index_left(row,first_non_null_index+1)
    #     if next_non_null_index!=len_row and row[next_non_null_index]==row[first_non_null_index]:
    #         row[iteration_number]=row[first_non_null_index]+row[next_non_null_index]
    #         row[first_non_null_index],row[next_non_null_index]=0,0
    #         first_non_null_index=next_non_null_index+1
    #     elif next_non_null_index==len_row:
    #         row[iteration_number]=row[first_non_null_index]
    #         row[first_non_null_index]=0
    #     first_non_null_index+=1
    #     iteration_number+=1
    # return row
    #
    #
    #

def reverse_row(row):
    """Retourne la ligne"""
    reversed_row=[]
    len_row=len(row)
    for tile_index in range(len_row):
        reversed_row.append(row[len_row-1-tile_index])
    return reversed_row

def move_row_right(row):
    """Renvoie l'effet d'une commande "droite" sur la grille"""
    row=reverse_row(row)
    row=move_row_left(row)
    row=reverse_row(row)
    return row

#move_row_right([2, 2, 0, 4])

def column_to_line(grid,column_index):
    """Renvoie la colonne d'indice column_index de la grille sous forme de ligne"""
    line=[]
    for line_index in range(len(grid)):
        line.append(grid[line_index][column_index])
    return line

def line_to_column(grid,column_index,line):
    """Insère la ligne line sous forme de colonne d'index column_index dans la grille"""
    for line_index in range(len(line)):
         grid[line_index][column_index]=line[line_index]
    return grid

def move_grid(grid,d):
    """Effet du mouvement selon la direction d sur la grille"""
    size_grid=len(grid)
    if d=="left":
        for line_index in range(size_grid):
            grid[line_index]=move_row_left(grid[line_index])
        return grid
    elif d=="right":
        for line_index in range(size_grid):
            grid[line_index]=move_row_right(grid[line_index])
        return grid
    elif d=="up":
        for column_index in range(size_grid):
            lined_column=column_to_line(grid,column_index)
            lined_column=move_row_left(lined_column)
            line_to_column(grid,column_index,lined_column)
        return grid
    elif d=="down":
        for column_index in range(size_grid):
            lined_column=column_to_line(grid,column_index)
            lined_column=move_row_right(lined_column)
            line_to_column(grid,column_index,lined_column)
        return grid

#print(move_grid([[2,0,0,2], [4, 4, 0, 0], [8, 0, 8, 0], [0, 2, 2, 0]],"left"))
#print(move_grid([[2,0,0,2], [4, 4, 0, 0], [8, 0, 8, 0], [0, 2, 2, 0]],"right"))

#Fonctionnalité 5:

def is_grid_full(grid):
    """Renvoie un booléen indiquant si la grille est pleine ou non"""
    if 0 in get_all_tiles(grid):
        return False
    else:
        return True
#print(move_grid([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]],"up"))
#print(move_grid([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]],"down"))

def move_possible(grid):
    """Renvoie une liste de booléen indquant si le mouvement de la grille est possible dans la direction correspondant [gauche,droite,haut,bas]"""
    possible_list=[]
    copy_grid1=[line[:] for line in grid]
    copy_grid2=[line[:] for line in grid]
    copy_grid3=[line[:] for line in grid]
    copy_grid4=[line[:] for line in grid]
    if move_grid(copy_grid1,"left")==grid:
        possible_list.append(False)
    else:
        possible_list.append(True)
    if grid==move_grid(copy_grid2,"right"):
        possible_list.append(False)
    else:
        possible_list.append(True)
    if move_grid(copy_grid3,"up")==grid:
        possible_list.append(False)
    else:
        possible_list.append(True)
    if move_grid(copy_grid4,"down")==grid:
        possible_list.append(False)
    else:
        possible_list.append(True)
    return possible_list

# print([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]])
# print(move_grid([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]],"up"))
# print(move_possible([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]]))
# print([[2, 2, 2, 2], [8, 16, 8, 16], [0, 8, 16, 4], [0, 0, 0, 32]]==[[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]])


def is_game_over(grid):
    """Renvoie un booléen qui indique si le jeu est fini ou non"""
    if move_possible(grid)==[False,False,False,False]:
        return True
    else:
        return False

def get_grid_tile_max(grid):
    "Renvoie la valeur maximale des tuiles de la grille"
    list_of_tiles=get_all_tiles(grid)
    tile_max=list_of_tiles[0]
    for tile in list_of_tiles:
        if tile>tile_max:
            tile_max=tile
    return tile_max


#Fonctions additionnelles

def fill_the_tiles(grid,d):
    """Fonction qui ajoute une nouvelle tuile non vide pour un mouvement d considéré avec une certaine probabilité dans les tuiles concernées"""
    all_empty_tiles=get_empty_tiles_positions(grid)
    size_grid=len(grid)
    if d=="left":
        for tile in all_empty_tiles:
            if tile[1]==size_grid-1 and random.choices([True,False],[0.3,0.7])[0]:
                grid[tile[0]][tile[1]]=random.choices([2,4],[0.9,0.1])[0]
    if d=="right":
        for tile in all_empty_tiles:
            if tile[1]==0 and random.choices([True,False],[0.3,0.7])[0]:
                grid[tile[0]][tile[1]]=random.choices([2,4],[0.9,0.1])[0]
    if d=="up":
        for tile in all_empty_tiles:
            if tile[0]==size_grid-1 and random.choices([True,False],[0.3,0.7])[0]:
                grid[tile[0]][tile[1]]=random.choices([2,4],[0.9,0.1])[0]
    if d=="down":
        for tile in all_empty_tiles:
            if tile[0]==0 and random.choices([True,False],[0.3,0.7])[0]:
                grid[tile[0]][tile[1]]=random.choices([2,4],[0.9,0.1])[0]
    return grid

# def move_row_right_without_sum(row):
#     """Mouvement vers la droite sans somme"""
#     row=reverse_row(row)
#     row=move_row_left_without_sum(row)
#     row=reverse_row(row)
#     return row
#
# def move_grid_without_sum(grid,d):
#     """Effet du mouvement selon la direction d sur la grille sans somme"""
#     size_grid=len(grid)
#     if d=="left":
#         for line_index in range(size_grid):
#             grid[line_index]=move_row_left_without_sum(grid[line_index])
#         return grid
#     elif d=="right":
#         for line_index in range(size_grid):
#             grid[line_index]=move_row_right_without_sum(grid[line_index])
#         return grid
#     elif d=="up":
#         for column_index in range(size_grid):
#             lined_column=column_to_line(grid,column_index)
#             lined_column=move_row_left_without_sum(lined_column)
#             line_to_column(grid,column_index,lined_column)
#         return grid
#     elif d=="down":
#         for column_index in range(size_grid):
#             lined_column=column_to_line(grid,column_index)
#             lined_column=move_row_right_without_sum(lined_column)
#             line_to_column(grid,column_index,lined_column)
#         return grid

