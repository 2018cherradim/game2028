from tkinter import *
from game2048.grid_2048 import *
#from game2048.play_2048 import *
from pprint import pformat
from functools import partial

size_grid = 4
TILES_BG_COLOR = {0: "#9e948a", 2: "#eee4da", 4: "#ede0c8", 8: "#f1b078", \
                  16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b", \
                  128: "#edcf72", 256: "#edcc61", 512: "#edc850", \
                  1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92", \
                  8192: "#24ba63"}

TILES_FG_COLOR = {0: "#776e65", 2: "#776e65", 4: "#776e65", 8: "#f9f6f2", \
                  16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", \
                  256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2", \
                  2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}

TILES_FONT = {"Verdana", 40, "bold"}


def graphical_grid_2048_init():
    global grid_2048_size
    global TILES_BG_COLOR
    global TILES_FG_COLOR
    root=Tk()
    root.title("2048")
    window=Toplevel(bg='yellow',width=grid_2048_size*100,height=grid_2048_size*100)
    window.title("2048")
    background=Frame(bg="yellow",height=grid_2048_size*100,width=grid_2048_size*100)
    graphical_grid=[]
    for i in range(grid_2048_size):
        for j in range(grid_2048_size):
            frame=Frame(window,bd=2,bg=TILES_FG_COLOR.get(0))
            Label(frame,padx=100,pady=100,bg=TILES_BG_COLOR.get(0)).grid(row=i,column=j)
            frame.place(x=i*100,y=j*100)
    window.mainloop()
    root.mainloop()

def key_pressed(event):
  event.bind('<KeyPress>',display_and_update_graphical_grid)
  if event == event.keysym_num==65433:
      print('bonnjur')

def display_and_update_graphical_grid():
    global size_grid
    global TILES_BG_COLOR
    global TILES_FG_COLOR
    grid=init_game(size_grid)
    root=Tk()
    root.title("2048")
    window=Toplevel(bg='yellow',width=size_grid*100,height=size_grid*100)
    window.title("2048")
  #  background=Frame(bg="yellow",height=size_grid*100,width=size_grid*100)
   # graphical_grid=[]
    for i in range(size_grid):
        for j in range(size_grid):
            if grid[i][j]==0:
                frame=Frame(window,bd=2,bg=TILES_FG_COLOR.get(0))
                frame.grid(row=i,column=j)
                Label(frame,bg=TILES_BG_COLOR.get(grid[i][j]),text=' ',font=("Verdana",40,"bold"),width=4,height=2).pack()
            else:
                frame=Frame(window,bd=2,bg=TILES_FG_COLOR.get(0))
                frame.grid(row=i,column=j)
                Label(frame,bg=TILES_BG_COLOR.get(grid[i][j]),text=str(grid[i][j]),font=("Verdana",40,"bold"),fg=TILES_FG_COLOR.get(grid[i][j]),width=4,height=2).pack()
    window.bind('<KeyPress>',key_pressed)
    window.mainloop()
    root.mainloop()


display_and_update_graphical_grid()

# graphical_grid_2048_init()
#display_and_update_graphical_grid([[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 4096], [1024, 2048, 512, 2]])
#display_and_update_graphical_grid([[2, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2], [0, 0, 0, 0]])




